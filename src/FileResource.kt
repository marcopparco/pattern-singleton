class FileResource constructor() {
    private val clave: Singleton = Singleton.getInstance()

    fun doFileOperation() {
        println("Doing file operation")
        clave.put("file")
        println("File operation completed with hashcode *${Singleton.hashCode()}*")
    }
}