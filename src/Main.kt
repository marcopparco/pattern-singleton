object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val singleton: Singleton = Singleton.getInstance()
        println("Initial Singleton instance count is ${Singleton.myInstancesCount}")
        println("")
        val networkRes = NetworkResource()
        networkRes.doNetworkOperation()
        println("Initial Singleton instance count is ${Singleton.myInstancesCount}")
        println("")
        val fileRes=FileResource()
        fileRes.doFileOperation()
        println("Initial Singleton instance count is ${Singleton.myInstancesCount}")
        println("")
        println("Main call singleton hashcode *${singleton.hashCode()}*")

        println("File operation value: ${singleton.get("file")}")
        println("Network operation value: ${singleton.get("network")}")
        println("Main call singleton hashcode after retrieving values *${singleton.hashCode()}*")

    }

}