import java.lang.IllegalArgumentException

class Singleton private constructor(){
    private var clave = ""
    init {
        ++myInstancesCount
    }

    companion object {
        //Debuggable field to check instance count
        var myInstancesCount = 0;
        private val mInstance: Singleton = Singleton()

        @Synchronized
        fun getInstance(): Singleton {
            return mInstance
        }
    }

    fun put(clave:String){
        when{
            !clave.isEmpty()-> this.clave=clave
            else-> throw IllegalArgumentException("Ingresar clave valida")
        }
    }

    fun get(clave: String):String?{
        when{
            this.clave===clave -> return clave
            else -> return "network"
        }
    }


}
/*

object Singleton {

    private var clave = ""


    fun put(clave:String){
        when{
            !clave.isEmpty()-> this.clave=clave
            else-> throw IllegalArgumentException("Ingresar clave valida")
        }
    }

    fun get(clave: String):String?{
        when{
            this.clave===clave -> return clave
            else -> return null
        }
    }
}
fun main(args: Array<String>) {


}*/

