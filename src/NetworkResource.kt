class NetworkResource constructor() {
    private val clave: Singleton = Singleton.getInstance()
    fun doNetworkOperation() {
        println("Doing network operation")
        clave.put("network")
        println("Network operation completed with hashcode *${Singleton.hashCode()}*")
    }
}